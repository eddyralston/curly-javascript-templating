var curly = {
    string(template, data){
        return template.replace(/{{(.*?)}}/g, (match) => {
          var str = data[match.split(/{{|}}/)[1]]
          if(str){return str}else{return ''}
        })
      },
    element(element){
        var template=element.innerHTML
        function data(obj){element.innerHTML=curly.string(template,obj)}
        return {data}
    }
}
